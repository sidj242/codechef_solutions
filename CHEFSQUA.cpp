    #include <bits/stdc++.h>
     
    using namespace std;
     
    #define MAX 1000006
    #define MOD 1000000007
    int main()
    {
    	int n,a,b;
        cin>>n;
        map < pair<int , int > , int > mp;
        map < pair<int , int > , int > :: iterator it;
        vector <pair<int , int > > vec;
        for(int i=1;i<=n;i++){
            cin>>a>>b;
            vec.push_back(make_pair(a,b));
            mp[make_pair(a,b)]=i;
     
        }
        if(n==0){
            cout<<"4";
            return 0;
        }
        if(n==1){
            cout<<"3";
            return 0;
        }
     
        int ans=4;
        for(int i=0;i<n-1;i++){
     
            for(int j=i+1;j<n;j++){
     
     
                pair<int , int > x= make_pair(vec[j].first-vec[i].first , vec[j].second-vec[i].second);
     
                pair<int , int > y= make_pair(x.second,-x.first);
                pair<int , int > z= make_pair(-x.second,x.first);
     
                double dis = sqrt(x.first*x.first + x.second*x.second);
     
                    pair<int , int > p,q;
                    p=vec[i];
                    q=vec[j];
     
                 p= make_pair(vec[i].first + y.first , vec[i].second + y.second);
                 q= make_pair(vec[j].first + y.first , vec[j].second + y.second);
     
     
                bool l= (mp.count(p)) ? true : false;
                bool m= (mp.count(q)) ? true : false;
     
                if(l && m) ans=min(ans,0);
                else if(l) ans=min(ans,1);
                else if (m) ans=min(ans,1);
                else ans=min(ans,2);
     
                y=z;
     
                p= make_pair(vec[i].first + y.first , vec[i].second + y.second);
                q= make_pair(vec[j].first + y.first , vec[j].second + y.second);
     
                l= (mp.count(p)) ? true : false;
                m= (mp.count(q)) ? true : false;
     
                if(l && m) ans=min(ans,0);
                else if(l) ans=min(ans,1);
                else if (m) ans=min(ans,1);
                else ans=min(ans,2);
     
            }
     
        }
        cout<<ans<<endl;
     
        return 0;
    }
     

Comments
Need help? Post a comment. But before that please spare a moment to read the guidelines.
Your name:
sidj242
Comment: *
